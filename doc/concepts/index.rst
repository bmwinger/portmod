Concepts
========

These are various concepts relevant to understanding how portmod works. It is recommended that you familiarise yourself with them before using Portmod.

.. list-table::
   :widths: 25 75
   :header-rows: 0

   * - :ref:`keywords`
     - .. summary:: keywords.rst
   * - :ref:`sets`
     - .. summary:: sets.rst
   * - :ref:`use-flags`
     - .. summary:: use-flags.rst
   * - :ref:`concepts-profiles`
     - .. summary:: profiles.rst
   * - :ref:`sandbox`
     - .. summary:: sandbox.rst
   * - :ref:`cfg-protect`
     - .. summary:: cfg-protect.rst
   * - :ref:`concepts-modules`
     - .. summary:: modules.rst

.. toctree::
   :hidden:

   keywords
   sets
   use-flags
   profiles
   sandbox
   cfg-protect
   modules
